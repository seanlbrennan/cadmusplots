# CadmusPlots

This R package contains functions that enable the user to create plots with Cadmus figure styles, including fonts and colors. See `demo/Demonstration Script.R` for a more comprehensive demo of the code, including installation and configuration.

## Installation Instructions

In RStudio, run this code:

```
# Enter your GitLab user name here.
gitlab_username = 'your.name'

# You'll need devtools to install the package. If you like, you can add any other
# dependencies in your code to this list to make sure they all get installed.
libraryList <- c('devtools', 'getPass', 'git2r', 'extrafont')

# Install any R package dependencies specified above that aren't already installed
if(any(!libraryList %in% library()$results)){
	install.packages(libraryList[which(!libraryList %in% library()$results)])
}

# Install CadmusPlots
devtools::install_git(
  "https://gitlab.com/ari.kornelis/cadmusplots.git",
  credentials = git2r::cred_user_pass(gitlab_username, getPass::getPass())
)

# If previous fails, try this:
devtools::install_git(
  "https://gitlab.com/ari.kornelis/cadmusplots.git"
)

## Test if the install was successful:
library(CadmusPlots)
library(ggplot2)

get_calibri()
prep_fonts()

# Sample Data:
data = data.frame(x = 1:10, y = 1:10)

ggplot(data) +
  geom_col(aes(x, y, fill = factor(x))) +
  theme_cadmus() +
  scale_fill_cadmus(palette = 'cool6')

```

If CadmusPlots is not already installed, you will be asked to input your GitLab password to complete the installation.
